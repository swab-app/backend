package application

import (
	"gitlab.com/swab-app/backend/domain"
)

type IRoutineRepository interface {
	GetRoutinesByUserId(userId int) ([]domain.Routine, error)
	GetRoutineByIdAndUserId(id, userId int, withRelations bool) (domain.Routine, error)
	CreateRoutine(routine domain.Routine) error
	SaveRoutine(routine domain.Routine) error
}
