package application

import (
	"gitlab.com/swab-app/backend/domain"
)

type IExerciseService interface {
	SearchExercises(name, searchType string) ([]domain.Exercise, error)
}

type ExerciseService struct {
	Repository IExerciseRepository
}

func (service ExerciseService) SearchExercises(name, searchType string) ([]domain.Exercise, error) {
	return service.Repository.Search(name, searchType)
}
