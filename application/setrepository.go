package application

type ISetRepository interface {
	DeleteSetsByRoutineExerciseId(routineExerciseIds []uint) error
}
