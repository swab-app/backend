package application

import (
	"errors"
	"testing"

	"gitlab.com/swab-app/backend/domain"
	"gitlab.com/swab-app/backend/util"
)

type FakeUserRepo struct {
	GetUserCalls           int
	GetUserByUserNameCalls int
	CreateUserCalls        int
}

func (r FakeUserRepo) GetUser(id uint) (domain.User, error) {
	r.GetUserCalls++
	return domain.User{ID: 5432, UserName: "foobarboiz"}, nil
}

func (r FakeUserRepo) GetUserByUserName(username string) (domain.User, error) {
	r.GetUserByUserNameCalls++
	if username == "ErrorPlease" {
		return domain.User{}, errors.New("Error happened")
	} else if username == "NoUserFoundPlease" {
		return domain.User{}, nil
	} else {
		return domain.User{ID: 5432, UserName: "foobarboiz"}, nil
	}
}

func (r FakeUserRepo) CreateUser(user domain.User) (domain.User, error) {
	r.CreateUserCalls++
	return domain.User{ID: 5432, UserName: "foobarboiz"}, nil
}

func TestWhenServiceGetUserByUserNameCalled_UserIsReturned(t *testing.T) {
	fakeRepository := FakeUserRepo{}
	service := UserService{Repository: fakeRepository, JwtKey: "abc123"}

	user, err := service.GetUserByUserName("foobarboiz")

	if user.ID != 5432 && fakeRepository.GetUserByUserNameCalls != 1 && err != nil {
		t.Fatalf("Returned user is empty")
	}
}

func TestWhenServiceFailsToFindUser_CorrectErrorIsReturned(t *testing.T) {
	fakeRepository := FakeUserRepo{}
	service := UserService{Repository: fakeRepository, JwtKey: "abc123"}

	user, err := service.GetUserByUserName("NoUserFoundPlease")

	if user.ID != 0 && err.Error() != "username or password was incorrect" {
		t.Fatalf("User returned or incorrect error on zero users.")
	}
}

func TestWhenServiceCallsRepoAndGetsError_ThenThatErrorIsReturned(t *testing.T) {
	fakeRepository := FakeUserRepo{}
	service := UserService{Repository: fakeRepository, JwtKey: "abc123"}

	user, err := service.GetUserByUserName("ErrorPlease")

	if user.ID != 0 && err.Error() != "Error happened" {
		t.Fatalf("User returned or repo error not returned.")
	}
}

func TestAuthAttemptWithValidPassword_ThenReturnsTrue(t *testing.T) {
	fakeRepository := FakeUserRepo{}
	service := UserService{Repository: fakeRepository, JwtKey: "SaibaQueIssoEmMimProvocaImensaDo"}
	password := "pass"
	user := domain.User{Password: "$2a$10$eMPSjDawbphhZ9IqWqlgC..duh1HvOc7fMhdLYc3oV/3rEgKVngra"}

	result := service.AttemptAuth(user, util.Password(password))

	if !result {
		t.Fatalf("User auth failed with correct pass")
	}
}
func TestAuthAttemptWithInvalidPassword_ThenReturnsFalse(t *testing.T) {
	fakeRepository := FakeUserRepo{}
	service := UserService{Repository: fakeRepository, JwtKey: "SaibaQueIssoEmMimProvocaImensaDo"}
	password := "psass"
	user := domain.User{Password: "$2a$10$eMPSjDawbphhZ9IqWqlgC..duh1HvOc7fMhdLYc3oV/3rEgKVngra"}

	result := service.AttemptAuth(user, util.Password(password))

	if result {
		t.Fatalf("User auth passed with incorrect pass")
	}
}

func TestCreateUserSuccess_ThenUserIsReturned(t *testing.T) {
	fakeRepository := FakeUserRepo{}
	service := UserService{Repository: fakeRepository, JwtKey: "abc123"}
	inputUser := domain.User{
		ID:       123,
		UserName: "foobarboiz",
	}

	user, err := service.CreateUser(inputUser)

	if user.ID == 0 && err != nil {
		t.Fatalf("User not saved, or error occurred when it shouldn't have")
	}
}
