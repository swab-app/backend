package application

import (
	"gitlab.com/swab-app/backend/dtos"
	"testing"

	"gitlab.com/swab-app/backend/domain"
)

// FAKE ROUTINE REPO START
type FakeRoutineRepo struct{}

func (r FakeRoutineRepo) GetRoutinesByUserId(userId int) ([]domain.Routine, error) {
	return []domain.Routine{
		{ID: 1, Name: "sample-routine", UserID: userId},
	}, nil
}

func (r FakeRoutineRepo) GetRoutineByIdAndUserId(id, userId int, withRelations bool) (domain.Routine, error) {
	return domain.Routine{ID: uint(id), Name: "sample-routine", UserID: userId}, nil
}

func (r FakeRoutineRepo) CreateRoutine(routine domain.Routine) error {
	return nil
}

func (r FakeRoutineRepo) SaveRoutine(routine domain.Routine) error {
	return nil
}

// FAKE ROUTINE REPO END

// FAKE ROUTINE EXERCISE REPO START
type FakeRoutineExerciseRepo struct{}

func (r FakeRoutineExerciseRepo) GetRoutineExercisesByRoutine(routineId int) ([]domain.RoutineExercise, error) {
	return []domain.RoutineExercise{
		{ID: 1, ExerciseId: 1, RoutineId: routineId, Order: 0},
	}, nil
}

func (r FakeRoutineExerciseRepo) GetRoutineExerciseIdsByRoutine(routineId int) ([]uint, error) {
	return []uint{1, 2, 3}, nil
}

func (r FakeRoutineExerciseRepo) DeleteRoutineExercisesByRoutine(rountineId int) error {
	return nil
}

// FAKE ROUTINE EXERCISE REPO END

// FAKE SET REPO START
type FakeSetRepo struct {
}

func (r FakeSetRepo) DeleteSetsByRoutineExerciseId(routineExerciseIds []uint) error {
	return nil
}

// FAKE SET REPO END

func GetTestRoutineService() RoutineService {
	routineRepo := FakeRoutineRepo{}
	routineExerciseRepo := FakeRoutineExerciseRepo{}
	setRepo := FakeSetRepo{}
	routineService := RoutineService{
		RoutineRepository:         routineRepo,
		RoutineExerciseRepository: routineExerciseRepo,
		SetRepository:             setRepo,
	}
	return routineService
}

func TestWhenGetRoutinesByUserIdIsCalled_CorrectRoutinesReturned(t *testing.T) {
	routineService := GetTestRoutineService()

	routines, err := routineService.GetRoutinesByUserId(1)

	if routines[0].UserID != 1 || err != nil {
		t.Fatalf("Wrong routine returned or error occurred")
	}
}
func TestWhenGetRoutineByIdAndUserIdIsCalled_CorrectRoutineReturned(t *testing.T) {
	routineService := GetTestRoutineService()

	routine, err := routineService.GetRoutineByIdAndUserId(1, 1, false)

	if routine.UserID != 1 || err != nil {
		t.Fatalf("Wrong routine returned or error occurred")
	}
}

func TestWhenCreateRoutineIsCalled_ThenNoErrorOccurs(t *testing.T) {
	routineService := GetTestRoutineService()

	err := routineService.CreateRoutine(domain.Routine{})

	if err != nil {
		t.Fatalf("An error occurred")
	}
}

func TestWhenUpdateRoutineIsCalled_ThenNoErrorOccurs(t *testing.T) {
	routineService := GetTestRoutineService()

	err := routineService.UpdateRoutine(domain.Routine{})

	if err != nil {
		t.Fatalf("An error occurred")
	}
}

func TestCreateRoutineFromDto_ThenCorrectDetailsAreReturned(t *testing.T) {
	createDto := dtos.RoutineCreateRequestDto{
		Name: "TestName",
		Exercises: []dtos.RoutineExerciseCreateRequestDto{
			{
				ExerciseId: 1,
				Sets: []dtos.SetCreateRequestDto{
					{
						Weight:   5,
						Time:     0,
						Reps:     5,
						Type:     "REPS",
						Distance: 0,
					},
					{
						Weight:   5,
						Time:     0,
						Reps:     5,
						Type:     "REPS",
						Distance: 0,
					},
				},
			},
		},
	}
	routineService := GetTestRoutineService()

	routine := routineService.CreateRoutineFromDto(4, createDto)

	routineMappedCorrectly := routine.UserID == 4 &&
		routine.Name == "TestName" &&
		len(routine.RoutineExercises[0].Sets) == 2

	if !routineMappedCorrectly {
		t.Fatal("Mapping incorrect")
	}
}

func TestWhenUpdateRoutineFromDto_ThenCorrectDetailsAreCorrect(t *testing.T) {
	updateDto := dtos.RoutineUpdateRequestDto{
		Name: "DtoRoutineName",
		Exercises: []dtos.RoutineExerciseUpdateRequestDto{
			{
				ExerciseId: 1,
				Sets: []dtos.SetUpdateRequestDto{
					{
						Weight:   5,
						Time:     0,
						Reps:     5,
						Type:     "REPS",
						Distance: 0,
					},
					{
						Weight:   5,
						Time:     0,
						Reps:     5,
						Type:     "REPS",
						Distance: 0,
					},
				},
			},
		},
	}
	domainRoutine := domain.Routine{
		ID:               466,
		Name:             "DomainRoutineName",
		RoutineExercises: []domain.RoutineExercise{},
		UserID:           455,
	}
	routineService := GetTestRoutineService()

	domainRoutine = routineService.UpdateRoutineFromDto(domainRoutine, updateDto)

	mappingValid := domainRoutine.Name == "DtoRoutineName" &&
		domainRoutine.RoutineExercises[0].Sets[0].Weight == 5
	if !mappingValid {
		t.Fatal("Mapping was incorrect")
	}
}
