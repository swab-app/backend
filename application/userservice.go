package application

import (
	"errors"
	"log"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/swab-app/backend/domain"
	"gitlab.com/swab-app/backend/util"
)

type IUserService interface {
	CreateUser(user domain.User) (domain.User, error)
	AttemptAuth(user domain.User, password util.Password) bool
	GetToken(user domain.User) (string, error)
	GetUserByUserName(username string) (domain.User, error)
}

type UserService struct {
	Repository IUserRespository
	JwtKey     string
}

func (service UserService) CreateUser(user domain.User) (domain.User, error) {
	user.Password = util.Password(user.Hash())
	return service.Repository.CreateUser(user)
}

func (service UserService) GetToken(user domain.User) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user": user.ID,
		"exp":  time.Now().Add(time.Hour * time.Duration(12)).Unix(),
		"iat":  time.Now().Unix(),
	})
	return token.SignedString([]byte(service.JwtKey))
}

func (service UserService) AttemptAuth(user domain.User, password util.Password) bool {
	return password.CheckAgainstHash(string(user.Password))
}

func (service UserService) GetUserByUserName(username string) (domain.User, error) {
	user, err := service.Repository.GetUserByUserName(username)
	if user == (domain.User{}) {
		log.Printf("Failed to find user with name %s", username)
		return user, errors.New("username or password was incorrect")
	}

	if err != nil {
		log.Printf("db error retrieving user %s", err)
		return user, err
	}
	return user, nil
}
