package application

import (
	"gitlab.com/swab-app/backend/domain"
	"gitlab.com/swab-app/backend/dtos"
)

type IRoutineService interface {
	GetRoutinesByUserId(userId int) ([]domain.Routine, error)
	GetRoutineByIdAndUserId(id, userId int, withRelations bool) (domain.Routine, error)
	CreateRoutine(routine domain.Routine) error
	UpdateRoutine(routine domain.Routine) error
	CreateRoutineFromDto(userId int, createRoutineDto dtos.RoutineCreateRequestDto) domain.Routine
	UpdateRoutineFromDto(domainRoutine domain.Routine, dto dtos.RoutineUpdateRequestDto) domain.Routine
}

type RoutineService struct {
	RoutineRepository         IRoutineRepository
	RoutineExerciseRepository IRoutineExerciseRepository
	SetRepository             ISetRepository
}

func (service RoutineService) GetRoutinesByUserId(userId int) ([]domain.Routine, error) {
	return service.RoutineRepository.GetRoutinesByUserId(userId)
}

func (service RoutineService) GetRoutineByIdAndUserId(id, userId int, withRelations bool) (domain.Routine, error) {
	return service.RoutineRepository.GetRoutineByIdAndUserId(id, userId, withRelations)
}

func (service RoutineService) CreateRoutine(routine domain.Routine) error {
	return service.RoutineRepository.CreateRoutine(routine)
}

func (service RoutineService) UpdateRoutine(routine domain.Routine) error {
	// find routine exercise ids
	routineExerciseIds, err := service.RoutineExerciseRepository.
		GetRoutineExerciseIdsByRoutine(int(routine.ID))
	if err != nil {
		return err
	}

	// delete the old sets of those routine exercises.
	err = service.SetRepository.DeleteSetsByRoutineExerciseId(routineExerciseIds)
	if err != nil {
		return err
	}

	// delete the old routine exercises
	err = service.RoutineExerciseRepository.DeleteRoutineExercisesByRoutine(int(routine.ID))
	if err != nil {
		return err
	}

	// save the new routine with updated routine exercises, sets
	return service.RoutineRepository.SaveRoutine(routine)
}

func (service RoutineService) CreateRoutineFromDto(userId int, createRoutineDto dtos.RoutineCreateRequestDto) domain.Routine {
	routine := domain.Routine{
		Name:   createRoutineDto.Name,
		UserID: userId,
	}
	for routineExerciseOrder, routineExerciseDto := range createRoutineDto.Exercises {
		routineExercise := domain.RoutineExercise{
			ExerciseId: routineExerciseDto.ExerciseId,
			Order:      routineExerciseOrder,
		}
		for _, setDto := range routineExerciseDto.Sets {
			set := domain.Set{
				Weight: setDto.Weight,
				Time:   setDto.Time,
				Reps:   setDto.Reps,
				Type:   domain.ExerciseType(setDto.Type),
			}
			routineExercise.Sets = append(routineExercise.Sets, set)
		}
		routine.RoutineExercises = append(routine.RoutineExercises, routineExercise)
	}
	return routine
}

func (service RoutineService) UpdateRoutineFromDto(domainRoutine domain.Routine, dto dtos.RoutineUpdateRequestDto) domain.Routine {
	domainRoutine.Name = dto.Name
	domainRoutine.RoutineExercises = []domain.RoutineExercise{}
	for exerciseOrder, exerciseDto := range dto.Exercises {
		domainRoutineExercise := domain.RoutineExercise{
			ExerciseId: exerciseDto.ExerciseId,
			RoutineId:  int(domainRoutine.ID),
			Order:      exerciseOrder,
		}

		for _, setDto := range exerciseDto.Sets {
			domainSet := domain.Set{
				RoutineExerciseId: &exerciseDto.ExerciseId,
				Weight:            setDto.Weight,
				Time:              setDto.Time,
				Reps:              setDto.Reps,
				Type:              domain.ExerciseType(setDto.Type),
			}
			domainRoutineExercise.Sets = append(domainRoutineExercise.Sets, domainSet)
		}

		domainRoutine.RoutineExercises = append(domainRoutine.RoutineExercises, domainRoutineExercise)
	}

	return domainRoutine
}
