package application

import (
	"testing"
	"time"

	"gitlab.com/swab-app/backend/domain"
	"gitlab.com/swab-app/backend/dtos"
)

type FakeWorkoutRepo struct {
	SaveCalledTimes int
}

func (r FakeWorkoutRepo) SaveWorkout(workout domain.Workout) error {
	r.SaveCalledTimes++
	return nil
}

func GetTestWorkoutSerivce() WorkoutService {
	fakeWorkoutRepo := FakeWorkoutRepo{
		SaveCalledTimes: 0,
	}
	return WorkoutService{
		Repository: fakeWorkoutRepo,
	}
}

func TestWhenCreateDomainFromDtoIsCalled_CorrectDomainWorkoutReturned(t *testing.T) {
	workoutService := GetTestWorkoutSerivce()
	dto := dtos.WorkoutCreateRequestDto{
		Name:      "Afternoon fun",
		RoutineId: 55,
		StartTime: time.Date(2022, time.April, 1, 3, 30, 0, 0, time.UTC),
		EndTime:   time.Date(2022, time.April, 1, 3, 30, 0, 0, time.UTC),
		WorkoutExercises: []dtos.WorkoutExerciseCreateRequestDto{
			{
				ExerciseId: 555,
				Order:      0,
				Sets: []dtos.SetCreateRequestDto{
					{
						Weight:   5,
						Time:     0,
						Reps:     5,
						Type:     "REPS",
						Distance: 0,
					},
					{
						Weight:   6,
						Time:     0,
						Reps:     6,
						Type:     "REPS",
						Distance: 0,
					},
				},
			},
		},
	}
	userId := 5

	domain := workoutService.CreateWorkoutFromDto(userId, dto)
	valid := dto.Name == domain.Name &&
		dto.EndTime == domain.EndTime &&
		dto.WorkoutExercises[0].Sets[0].Weight == 5

	if !valid {
		t.Fatal("Mapping incorrect")
	}
}

func TestWhenCreateWorkoutIsCalled_ThenNoErrorsReturned(t *testing.T) {
	fakeWorkoutRepo := FakeWorkoutRepo{
		SaveCalledTimes: 0,
	}
	workoutService := WorkoutService{
		Repository: fakeWorkoutRepo,
	}
	workout := domain.Workout{
		Name:      "Flargeboi",
		StartTime: time.Date(2022, time.April, 1, 3, 30, 0, 0, time.UTC),
		EndTime:   time.Date(2022, time.April, 1, 3, 30, 0, 0, time.UTC),
		UserID:    4,
		RoutineId: 6,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	err := workoutService.Repository.SaveWorkout(workout)
	if err != nil && fakeWorkoutRepo.SaveCalledTimes != 1 {
		t.Fatalf("Error ocurred or didn't call downstream")
	}
}
