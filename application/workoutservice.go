package application

import (
	"gitlab.com/swab-app/backend/domain"
	"gitlab.com/swab-app/backend/dtos"
)

type IWorkoutService interface {
	CreateWorkoutFromDto(userId int, dto dtos.WorkoutCreateRequestDto) domain.Workout
	CreateWorkout(workout domain.Workout) error
}

type WorkoutService struct {
	Repository IWorkoutRepository
}

func (service WorkoutService) CreateWorkoutFromDto(userId int, dto dtos.WorkoutCreateRequestDto) domain.Workout {
	workout := domain.Workout{
		Name:             dto.Name,
		UserID:           userId,
		StartTime:        dto.StartTime,
		EndTime:          dto.EndTime,
		RoutineId:        dto.RoutineId,
		WorkoutExercises: []domain.WorkoutExercise{},
	}

	for _, workoutExerciseDto := range dto.WorkoutExercises {
		workoutExerciseDomain := domain.WorkoutExercise{
			ExerciseId: workoutExerciseDto.ExerciseId,
			Order:      workoutExerciseDto.Order,
			Sets:       []domain.Set{},
		}
		for _, setDto := range workoutExerciseDto.Sets {
			setDomain := domain.Set{
				Type:     domain.ExerciseType(setDto.Type),
				Weight:   setDto.Weight,
				Time:     setDto.Time,
				Reps:     setDto.Reps,
				Distance: setDto.Distance,
			}
			workoutExerciseDomain.Sets = append(workoutExerciseDomain.Sets, setDomain)
		}
		workout.WorkoutExercises = append(workout.WorkoutExercises, workoutExerciseDomain)
	}
	return workout
}

func (service WorkoutService) CreateWorkout(workout domain.Workout) error {
	return service.Repository.SaveWorkout(workout)
}
