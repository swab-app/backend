package application

import (
	"gitlab.com/swab-app/backend/domain"
)

type IRoutineExerciseRepository interface {
	GetRoutineExercisesByRoutine(routineId int) ([]domain.RoutineExercise, error)
	GetRoutineExerciseIdsByRoutine(routineId int) ([]uint, error)
	DeleteRoutineExercisesByRoutine(rountineId int) error
}
