package application

import (
	"errors"
	"testing"
	"time"

	"gitlab.com/swab-app/backend/domain"
)

type FakeExerciseRepo struct {
}

func (r FakeExerciseRepo) Search(name, searchType string) ([]domain.Exercise, error) {
	exercises := make([]domain.Exercise, 0)
	if name == "ERR" && searchType == "ERR" {
		return exercises, errors.New("error requested")
	}
	exercises = append(exercises, domain.Exercise{
		ID:        0,
		Name:      "Fart",
		Type:      domain.REPS,
		CreatedAt: time.Date(2000, 1, 1, 12, 30, 0, 0, time.Local),
		UpdatedAt: time.Date(2000, 1, 1, 12, 30, 0, 0, time.Local),
	})
	exercises = append(exercises, domain.Exercise{
		ID:        0,
		Name:      "Burp",
		Type:      domain.REPS,
		CreatedAt: time.Date(2000, 1, 1, 12, 30, 0, 0, time.Local),
		UpdatedAt: time.Date(2000, 1, 1, 12, 30, 0, 0, time.Local),
	})
	return exercises, nil
}

func TestWhenServiceSearchExercisesIsCalled_ExercisesAreReturned(t *testing.T) {
	fakeRepo := FakeExerciseRepo{}
	service := ExerciseService{Repository: fakeRepo}

	exercises, err := service.SearchExercises("abc", "123")

	if len(exercises) == 0 || err != nil {
		t.Fatalf("Error or no results returned :)")
	}
}

func TestWhenServiceSearchExercisesIsCalled_WhenErrorOccursInRepo_ThenErrorIsReturned(t *testing.T) {
	fakeRepo := FakeExerciseRepo{}
	service := ExerciseService{Repository: fakeRepo}

	exercises, err := service.SearchExercises("ERR", "ERR")

	if len(exercises) != 0 || err == nil {
		t.Fatalf("Error expected, none received")
	}
}
