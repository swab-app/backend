package application

import (
	"gitlab.com/swab-app/backend/domain"
)

type IExerciseRepository interface {
	Search(name, searchType string) ([]domain.Exercise, error)
}
