package application

import (
	"gitlab.com/swab-app/backend/domain"
)

type IUserRespository interface {
	GetUser(id uint) (domain.User, error)
	GetUserByUserName(username string) (domain.User, error)
	CreateUser(user domain.User) (domain.User, error)
}
