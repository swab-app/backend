package application

import "gitlab.com/swab-app/backend/domain"

type IWorkoutRepository interface {
	SaveWorkout(workout domain.Workout) error
}
