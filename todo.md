### todo 

### 31-03-22 

- just finished 'clean' arch refactor 
- now allow users to record a routine being done 

ui idea
    - user picks routine from home page or empty one
    - record start time, display empty sets and etc.
    - users fills them out, progress is saved locally as json string. 
    - sends something like this at end.

```
{
    "userId": number,
    "routineId" : optional number, 
    "startTime" : timestamp 
    "endTime" : timestamp 
    "exercises" : [
        {
            "exerciseId": number, 
            "order": number, 
            "sets" : [
                {
                    // see existing sets
                }
            ]
        }
    ]
}
```
