package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/swab-app/backend/api"
)

func serve() {
	s := api.Server{}
	err := s.Initialise()
	if err != nil {
		log.Fatal(err.Error())
	}
	s.Run("localhost:5312")
}

func usage() {
	fmt.Printf("%s {serve}\n", os.Args[0])
}

func main() {
	if len(os.Args) == 1 {
		usage()
		return
	}

	switch os.Args[1] {
	case "serve":
		serve()
	default:
		usage()
	}
}
