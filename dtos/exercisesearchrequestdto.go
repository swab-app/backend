package dtos

import (
	"net/url"
)

type ExerciseSearchRequestDto struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

func NewExerciseSearchRequestDto(url *url.URL) ExerciseSearchRequestDto {
	return ExerciseSearchRequestDto{
		Name: url.Query().Get("name"),
		Type: url.Query().Get("type"),
	}
}
