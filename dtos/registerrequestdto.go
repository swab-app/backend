package dtos

import "gitlab.com/swab-app/backend/util"

type RegisterRequestDto struct {
	UserName             string        `json:"username"`
	Password             util.Password `json:"password"`
	PasswordConfirmation util.Password `json:"passwordConfirmation"`
	Email                string        `json:"email"`
}
