package dtos

import "gitlab.com/swab-app/backend/domain"

type LoginResponseDto struct {
	User  domain.User
	Token string `json:"token"`
}
