package dtos

type SetCreateRequestDto struct {
	Weight   float32 `json:"weight"`
	Time     int     `json:"time"`
	Reps     int     `json:"reps"`
	Type     string  `json:"type"`
	Distance float32 `json:"distance"`
}

type SetUpdateRequestDto struct {
	Weight   float32 `json:"weight"`
	Time     int     `json:"time"`
	Reps     int     `json:"reps"`
	Type     string  `json:"type"`
	Distance float32 `json:"distance"`
}
