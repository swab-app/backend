package dtos

type RoutineExerciseCreateRequestDto struct {
	ExerciseId int                   `json:"id"`
	Sets       []SetCreateRequestDto `json:"sets"`
}

type RoutineExerciseUpdateRequestDto struct {
	ExerciseId int                   `json:"id"`
	Sets       []SetUpdateRequestDto `json:"sets"`
}

type RoutineCreateRequestDto struct {
	Name      string                            `json:"name"`
	Exercises []RoutineExerciseCreateRequestDto `json:"exercises"`
}

type RoutineUpdateRequestDto struct {
	Name      string                            `json:"name"`
	Exercises []RoutineExerciseUpdateRequestDto `json:"exercises"`
}
