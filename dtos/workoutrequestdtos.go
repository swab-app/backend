package dtos

import "time"

type WorkoutCreateRequestDto struct {
	Name             string                            `json:"name"`
	RoutineId        int                               `json:"routineId"`
	StartTime        time.Time                         `json:"startTime"`
	EndTime          time.Time                         `json:"endTime"`
	WorkoutExercises []WorkoutExerciseCreateRequestDto `json:"workoutExercises"`
}

type WorkoutExerciseCreateRequestDto struct {
	ExerciseId int                   `json:"id"`
	Order      int                   `json:"order"`
	Sets       []SetCreateRequestDto `json:"sets"`
}
