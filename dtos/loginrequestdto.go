package dtos

import (
	"gitlab.com/swab-app/backend/util"
)

type LoginRequestDto struct {
	UserName string `json:"username"`
	Password util.Password
}
