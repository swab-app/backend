package infrastructure

import (
	"gitlab.com/swab-app/backend/domain"
	"gorm.io/gorm"
)

type ExerciseRepository struct {
	DB *gorm.DB
}

func (repo ExerciseRepository) Search(name, searchType string) ([]domain.Exercise, error) {
	exercises := make([]domain.Exercise, 0)

	// TODO: check if this comment is still true, since refactoring this method out of controller.
	// need to make a copy of connection, or else the conditions perist between requests.
	var query = repo.DB
	if name != "" {
		query = query.Where("name LIKE ?", name)
	}

	if searchType != "" {
		query = query.Where("type = ?", searchType)
	}

	err := query.Find(&exercises).Error

	return exercises, err
}
