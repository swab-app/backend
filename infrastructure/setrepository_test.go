package infrastructure

import (
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
)

func TestWhenDeleteSetsCalled_ThenNoErrorIsReturned(t *testing.T) {
	db, mock, err := SetUpDbMock()
	if err != nil {
		t.Fatal("Failed to open mock db connection", err)
	}
	repo := SetRepository{DB: db}
	deleteStatement := "UPDATE `sets` SET `deleted_at`=? WHERE routine_exercise_id in (?) AND `sets`.`deleted_at` IS NULL"
	// formatString := "2006-01-02 15:04:05.000 -0700 MST"
	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta(deleteStatement)).
		WithArgs(AnyTime{}, 1).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	ids := []uint{1}
	err = repo.DeleteSetsByRoutineExerciseId(ids)

	if err != nil {
		t.Fatalf("Error has occurred")
	}
}
