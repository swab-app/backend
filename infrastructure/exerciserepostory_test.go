package infrastructure

import (
	"regexp"
	"testing"
	"time"

	"gitlab.com/swab-app/backend/domain"
)

func TestWhenCallingSearchExercises_ThenListOfExercisesAreReturned(t *testing.T) {
	db, mock, err := SetUpDbMock()
	if err != nil {
		t.Fatal("Failed to open mock db conn", err)
	}
	repo := ExerciseRepository{DB: db}
	exercises := []domain.Exercise{
		{
			ID:        1,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
			Name:      "Fart",
			Type:      domain.REPS,
		}, {
			ID:        2,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
			Name:      "Greep",
			Type:      domain.REPS,
		},
	}
	rows := mock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "name", "type"})
	for _, exercise := range exercises {
		rows.AddRow(exercise.ID, exercise.CreatedAt, exercise.UpdatedAt, nil, exercise.Name, exercise.Type)
	}
	const sqlSelectMany = "SELECT * FROM `exercises` WHERE name LIKE ? AND type = ? AND `exercises`.`deleted_at` IS NULL"
	mock.ExpectQuery(regexp.QuoteMeta(sqlSelectMany)).
		WithArgs("Fart", "REPS").
		WillReturnRows(rows)

	outputExercises, err := repo.Search("Fart", "REPS")

	if len(outputExercises) != 2 || err != nil {
		t.Fatal("Error occurred or no exercises found?", err)
	}
}

func TestWhenCallingSearchExercisesWithAnEmptyArgument_ThenListOfExercisesAreReturned_AndQueryContainsOnlyOneCondition(t *testing.T) {
	db, mock, err := SetUpDbMock()
	if err != nil {
		t.Fatal("Failed to open mock db conn", err)
	}
	repo := ExerciseRepository{DB: db}
	exercises := []domain.Exercise{
		{
			ID:        1,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
			Name:      "Fart",
			Type:      domain.REPS,
		}, {
			ID:        2,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
			Name:      "Greep",
			Type:      domain.REPS,
		},
	}
	rows := mock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "name", "type"})
	for _, exercise := range exercises {
		rows.AddRow(exercise.ID, exercise.CreatedAt, exercise.UpdatedAt, nil, exercise.Name, exercise.Type)
	}
	const sqlSelectMany = "SELECT * FROM `exercises` WHERE name LIKE ? AND `exercises`.`deleted_at` IS NULL"
	mock.ExpectQuery(regexp.QuoteMeta(sqlSelectMany)).
		WithArgs("Fart").
		WillReturnRows(rows)

	outputExercises, err := repo.Search("Fart", "")

	if len(outputExercises) != 2 || err != nil {
		t.Fatal("Error occurred or no exercises found?", err)
	}
}
