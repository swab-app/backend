package infrastructure

import (
	"gitlab.com/swab-app/backend/domain"
	"gorm.io/gorm"
)

type UserRepository struct {
	DB *gorm.DB
}

func (u UserRepository) GetUser(id uint) (domain.User, error) {
	user := domain.User{}
	err := u.DB.Find(&user, id).Error
	return user, err
}

func (u UserRepository) GetUserByUserName(username string) (domain.User, error) {
	user := domain.User{}
	err := u.DB.Where("username = ?", username).Find(&user).Error
	return user, err
}

func (u UserRepository) CreateUser(user domain.User) (domain.User, error) {
	err := u.DB.Create(&user).Error
	return user, err
}
