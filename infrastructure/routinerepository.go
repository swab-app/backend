package infrastructure

import (
	"gitlab.com/swab-app/backend/domain"
	"gorm.io/gorm"
)

type RoutineRepository struct {
	DB *gorm.DB
}

func (r RoutineRepository) GetRoutinesByUserId(userId int) ([]domain.Routine, error) {
	routines := make([]domain.Routine, 0)
	err := r.DB.Where("user_id = ?", userId).
		Preload("RoutineExercises.Exercise").
		Preload("RoutineExercises.Sets").
		Find(&routines).Error

	return routines, err
}

func (r RoutineRepository) GetRoutineByIdAndUserId(id, userId int, withRelations bool) (domain.Routine, error) {
	routine := domain.Routine{}
	query := r.DB.Where("user_id = ?", userId).
		Where("id = ?", id)

	if withRelations {
		query = query.Preload("RoutineExercises.Exercise").
			Preload("RoutineExercises.Sets")
	}

	err := query.Find(&routine).Error

	return routine, err
}

func (r RoutineRepository) CreateRoutine(routine domain.Routine) error {
	return r.DB.Create(&routine).Error
}

func (r RoutineRepository) SaveRoutine(routine domain.Routine) error {
	return r.DB.Save(&routine).Error
}
