package infrastructure

import (
	"gitlab.com/swab-app/backend/domain"
	"gorm.io/gorm"
)

type SetRepository struct {
	DB *gorm.DB
}

func (r SetRepository) DeleteSetsByRoutineExerciseId(routineExerciseIds []uint) error {
	return r.DB.Where("routine_exercise_id in ?", routineExerciseIds).Delete(&domain.Set{}).Error
}
