package infrastructure

import (
	"log"
	"regexp"
	"testing"
	"time"
)

func TestWhenGetRoutinesByUserIdCalled_CorrectRoutinesReturned(t *testing.T) {
	db, mock, err := SetUpDbMock()
	if err != nil {
		t.Fatal("Failed to open mock db connection", err)
	}
	repo := RoutineRepository{DB: db}
	rows := mock.
		NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "name", "user_id"}).
		AddRow(1, time.Now(), time.Now(), nil, "an-exercise", 1).
		AddRow(2, time.Now(), time.Now(), nil, "a-second-exercise", 1)
	const sqlSelect = "SELECT * FROM `routines` WHERE user_id = ? AND `routines`.`deleted_at` IS NULL"
	mock.ExpectQuery(regexp.QuoteMeta(sqlSelect)).
		WithArgs(1).
		WillReturnRows(rows)

	preloadRows := mock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "exercise_id", "routine_id", "order"})
	const preloadSelect = "SELECT * FROM `routine_exercises` WHERE `routine_exercises`.`routine_id` IN (?,?) AND `routine_exercises`.`deleted_at` IS NULL"
	mock.ExpectQuery(regexp.QuoteMeta(preloadSelect)).WithArgs(1, 2).WillReturnRows(preloadRows)
	mock.ExpectQuery(regexp.QuoteMeta(preloadSelect)).WithArgs(1, 2).WillReturnRows(preloadRows)

	actualRoutines, err := repo.GetRoutinesByUserId(1)

	if err != nil || len(actualRoutines) != 2 {
		log.Printf("output routine lenght %t", len(actualRoutines) != 2)
		log.Printf("err %s", err.Error())
		t.Fatalf("Error is present or wrong number of routine exercises found.")
	}
}

func TestWhenGetRoutineByIdandUserIdCalled_CorrectRoutinesAreReturned(t *testing.T) {
	db, mock, err := SetUpDbMock()
	if err != nil {
		t.Fatal("Failed to open mock db connection", err)
	}
	repo := RoutineRepository{DB: db}
	rows := mock.
		NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "name", "user_id"}).
		AddRow(1, time.Now(), time.Now(), nil, "an-exercise", 1).
		AddRow(2, time.Now(), time.Now(), nil, "a-second-exercise", 1)
	const sqlSelect = "SELECT * FROM `routines` WHERE user_id = ? AND id = ? AND `routines`.`deleted_at` IS NULL"
	mock.ExpectQuery(regexp.QuoteMeta(sqlSelect)).
		WithArgs(1, 1).
		WillReturnRows(rows)

	actualRoutine, err := repo.GetRoutineByIdAndUserId(1, 1, false)
	if err != nil || actualRoutine.ID == 0 {
		log.Printf("actual routine id = 0? %t", actualRoutine.ID == 0)
		log.Printf("err %s", err.Error())
		t.Fatalf("Error is present or routine not found")
	}
}
