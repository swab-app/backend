package infrastructure

import (
	"gitlab.com/swab-app/backend/domain"
	"gorm.io/gorm"
)

type RoutineExerciseRepository struct {
	DB *gorm.DB
}

func (r RoutineExerciseRepository) GetRoutineExercisesByRoutine(routineId int) ([]domain.RoutineExercise, error) {
	routineExercises := []domain.RoutineExercise{}
	err := r.DB.Where("routine_id = ?", routineId).Find(&routineExercises).Error
	return routineExercises, err
}

func (r RoutineExerciseRepository) GetRoutineExerciseIdsByRoutine(routineId int) ([]uint, error) {
	exercises, err := r.GetRoutineExercisesByRoutine(routineId)
	ids := []uint{}
	if err != nil {
		return ids, err
	}
	for _, ex := range exercises {
		ids = append(ids, ex.ID)
	}
	return ids, nil
}

func (r RoutineExerciseRepository) DeleteRoutineExercisesByRoutine(routineId int) error {
	return r.DB.Where("routine_id = ?", routineId).Delete(&domain.RoutineExercise{}).Error
}
