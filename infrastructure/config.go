package infrastructure

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type DBConfig struct {
	Dialect  string
	Username string
	Password string
	Name     string
	Charset  string
}

type Config struct {
	DB      *DBConfig
	AppName string
	JwtKey  string
}

func NewConfig() *Config {
	return &Config{
		AppName: "SWAB",
		DB: &DBConfig{
			Dialect:  "mysql",
			Username: "swab",
			Name:     "swab",
			Password: "pADxJIecVCJb9JG7",
			Charset:  "utf8mb4",
		},
		JwtKey: "SaibaQueIssoEmMimProvocaImensaDo",
	}
}

func (config Config) GetDatabase() (*gorm.DB, error) {
	return config.DB.GetDatabase()
}

func (dbConfig DBConfig) getDbUri() string {
	return fmt.Sprintf("%s:%s@/%s?charset=%s&parseTime=True&loc=Local",
		dbConfig.Username,
		dbConfig.Password,
		dbConfig.Name,
		dbConfig.Charset)
}

func (dbConfig DBConfig) GetDatabase() (*gorm.DB, error) {
	uri := dbConfig.getDbUri()

	db, err := gorm.Open(mysql.Open(uri), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	return db, err
}
