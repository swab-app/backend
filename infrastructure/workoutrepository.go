package infrastructure

import (
	"gitlab.com/swab-app/backend/domain"
	"gorm.io/gorm"
)

type WorkoutRepository struct {
	DB *gorm.DB
}

func (w WorkoutRepository) SaveWorkout(workout domain.Workout) error {
	return w.DB.Create(&workout).Error
}
