package infrastructure

import (
	"log"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/swab-app/backend/domain"
	"gitlab.com/swab-app/backend/util"
)

func TestWhenCallingGetUser_ThenUserIsReturned(t *testing.T) {
	db, sqlmock, err := SetUpDbMock()
	if err != nil {
		t.Fatal("Failed to open mock db connection", err)
	}
	repo := UserRepository{DB: db}
	user := domain.User{
		ID:        1,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		UserName:  "foobar",
		Email:     "foobar@lol.com",
		Password:  util.Password("alsfdlajksgdfljhg"),
	}
	rows := sqlmock.
		NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "username", "email", "password"}).
		AddRow(user.ID, user.CreatedAt, user.UpdatedAt, user.DeletedAt, user.UserName, user.Email, user.Password)
	const sqlSelectOne = "SELECT * FROM `users` WHERE `users`.`id` = ? AND `users`.`deleted_at` IS NULL"
	sqlmock.ExpectQuery(regexp.QuoteMeta(sqlSelectOne)).
		WithArgs(user.ID).
		WillReturnRows(rows)

	outputUser, err := repo.GetUser(1)

	if outputUser != user || err != nil {
		t.Fatal("Error occured or used was not found")
	}
}

func TestWhenCallingGetUserByUserName_ThenUserIsReturned(t *testing.T) {
	db, sqlmock, err := SetUpDbMock()
	if err != nil {
		t.Fatal("Failed to open mock db connection", err)
	}
	repo := UserRepository{DB: db}
	user := domain.User{
		ID:        1,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		UserName:  "foobar",
		Email:     "foobar@lol.com",
		Password:  util.Password("alsfdlajksgdfljhg"),
	}
	rows := sqlmock.
		NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "username", "email", "password"}).
		AddRow(user.ID, user.CreatedAt, user.UpdatedAt, user.DeletedAt, user.UserName, user.Email, user.Password)
	const sqlSelectOne = "SELECT * FROM `users` WHERE username = ? AND `users`.`deleted_at` IS NULL"
	sqlmock.ExpectQuery(regexp.QuoteMeta(sqlSelectOne)).
		WithArgs(user.UserName).
		WillReturnRows(rows)

	outputUser, err := repo.GetUserByUserName("foobar")

	if outputUser != user || err != nil {
		t.Fatal("Error occured or used was not found")
	}
}

// Todo: this is currently fucked due to password hasing automatically in `beforecreate` method.
func TestWhenCreateUserCalled_ThenUserIsReturned(t *testing.T) {
	db, sqlMock, err := SetUpDbMock()
	if err != nil {
		t.Fatal("Failed to open mock db connection", err)
	}
	repo := UserRepository{DB: db}
	pass := util.Password("alsfdlajksgdfljhg")
	// hash, _ := pass.Hash()
	user := domain.User{
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		UserName:  "foobar",
		Email:     "foobar@lol.com",
		Password:  pass,
	}

	insertStatement := "INSERT INTO `users` (`created_at`,`updated_at`,`deleted_at`,`username`,`email`,`password`) VALUES (?,?,?,?,?,?)"
	sqlMock.ExpectBegin()
	sqlMock.ExpectExec(regexp.QuoteMeta(insertStatement)).
		WithArgs(user.CreatedAt, user.UpdatedAt, nil, user.UserName, user.Email, user.Password).
		WillReturnResult(sqlmock.NewResult(1, 1))
	sqlMock.ExpectCommit()

	outputUser, err := repo.CreateUser(user)

	if outputUser != user && err != nil {
		log.Printf("output equals user: %t", outputUser == user)
		log.Printf("error is nil?  %t", err != nil)
		t.Fatal("User create failed or mismatch in return value")
	}
}
