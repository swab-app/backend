package infrastructure

import (
	"database/sql/driver"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func SetUpDbMock() (*gorm.DB, sqlmock.Sqlmock, error) {
	db, sqlmock, err := sqlmock.New()

	if err != nil {
		return nil, sqlmock, err
	}

	gormDb, err := gorm.Open(mysql.New(mysql.Config{
		Conn:                      db,
		SkipInitializeWithVersion: true,
	}), &gorm.Config{})
	if err != nil {
		return nil, sqlmock, err
	}

	return gormDb, sqlmock, nil
}

type AnyTime struct{}

func (a AnyTime) Match(v driver.Value) bool {
	_, ok := v.(time.Time)
	return ok
}
