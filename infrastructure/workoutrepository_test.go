package infrastructure

import (
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/swab-app/backend/domain"
)

func TestWhenCallingsSaveWorkout_ThenNoErrorIsReturned(t *testing.T) {
	db, sqlMock, err := SetUpDbMock()
	if err != nil {
		t.Fatal("Failed to open mock db connection", err)
	}
	repo := WorkoutRepository{DB: db}
	workout := domain.Workout{
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		Name:      "Island of the Shitty Monkeys",
		StartTime: time.Now(),
		EndTime:   time.Now().Add(time.Duration(time.Hour * 1)),
		UserID:    5,
		RoutineId: 0,
		WorkoutExercises: []domain.WorkoutExercise{
			{
				ExerciseId: 55,
				Order:      0,
				CreatedAt:  time.Now(),
				UpdatedAt:  time.Now(),
				Sets: []domain.Set{
					{
						CreatedAt:    time.Now(),
						UpdatedAt:    time.Now(),
						Weight:       50,
						Time:         0,
						Reps:         10,
						Distance:     0,
						DistanceUnit: "KM",
						Type:         "REPS",
					},
				},
			},
		},
	}

	insertWorkoutStatement := "INSERT INTO `workouts` (`created_at`,`updated_at`,`deleted_at`,`name`,`start_time`,`end_time`,`user_id`,`routine_id`) VALUES (?,?,?,?,?,?,?,?)"
	insertWorkoutExerciseStatement := "INSERT INTO `workout_exercises` (`exercise_id`,`workout_id`,`order`,`created_at`,`updated_at`,`deleted_at`) VALUES (?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `workout_id`=VALUES(`workout_id`)"
	insertSetStatement := "INSERT INTO `sets` (`created_at`,`updated_at`,`deleted_at`,`routine_exercise_id`,`type`,`workout_exercise_id`,`weight`,`time`,`reps`,`distance`,`distance_unit`) VALUES (?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `workout_exercise_id`=VALUES(`workout_exercise_id`)"
	wExercise := workout.WorkoutExercises[0]
	set := wExercise.Sets[0]
	sqlMock.ExpectBegin()
	sqlMock.ExpectExec(regexp.QuoteMeta(insertWorkoutStatement)).
		WithArgs(workout.CreatedAt, workout.UpdatedAt, nil, workout.Name, workout.StartTime, workout.EndTime, workout.UserID, 0).
		WillReturnResult(sqlmock.NewResult(1, 1))
	sqlMock.ExpectExec(regexp.QuoteMeta(insertWorkoutExerciseStatement)).
		WithArgs(55, 1, 0, wExercise.CreatedAt, wExercise.UpdatedAt, nil).
		WillReturnResult(sqlmock.NewResult(1, 1))
	sqlMock.ExpectExec(regexp.QuoteMeta(insertSetStatement)).
		WithArgs(set.CreatedAt, set.UpdatedAt, nil, nil, "REPS", 1, float64(50), 0, 10, float64(0), "KM").
		WillReturnResult(sqlmock.NewResult(1, 1))
	sqlMock.ExpectCommit()

	err = repo.SaveWorkout(workout)

	if err != nil {
		t.Fatal("Error has occurred", err)
	}
}
