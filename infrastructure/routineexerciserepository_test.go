package infrastructure

import (
	"log"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
)

func TestWhenGetRoutineExercisesByRoutineCalled_ThenRoutineExercisesReturned(t *testing.T) {
	db, mock, err := SetUpDbMock()
	if err != nil {
		t.Fatal("Failed to open mock db connection", err)
	}
	repo := RoutineExerciseRepository{DB: db}
	rows := mock.
		NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "exercise_id", "routine_id", "order"}).
		AddRow(1, time.Now(), time.Now(), nil, 1, 1, 0).
		AddRow(2, time.Now(), time.Now(), nil, 2, 1, 1)
	const sqlSelect = "SELECT * FROM `routine_exercises` WHERE routine_id = ? AND `routine_exercises`.`deleted_at` IS NULL"
	mock.ExpectQuery(regexp.QuoteMeta(sqlSelect)).
		WithArgs(1).
		WillReturnRows(rows)

	outputRoutineExercises, err := repo.GetRoutineExercisesByRoutine(1)

	if err != nil || len(outputRoutineExercises) != 2 {
		log.Printf("output routine exercises length, %t", len(outputRoutineExercises) != 2)
		log.Printf("err %s", err.Error())
		t.Fatalf("Error is present, or wrong number of routine exercises found")
	}
}

func TestWhenGetRoutineExerciseIdsByRoutineCalled_ThenRoutineExerciseIdsAreReturned(t *testing.T) {
	db, mock, err := SetUpDbMock()
	if err != nil {
		t.Fatal("Failed to open mock db connection", err)
	}
	repo := RoutineExerciseRepository{DB: db}
	rows := mock.
		NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "exercise_id", "routine_id", "order"}).
		AddRow(1, time.Now(), time.Now(), nil, 1, 1, 0).
		AddRow(2, time.Now(), time.Now(), nil, 2, 1, 1)
	const sqlSelect = "SELECT * FROM `routine_exercises` WHERE routine_id = ? AND `routine_exercises`.`deleted_at` IS NULL"
	mock.ExpectQuery(regexp.QuoteMeta(sqlSelect)).
		WithArgs(1).
		WillReturnRows(rows)

	outputRoutineExercisesIds, err := repo.GetRoutineExerciseIdsByRoutine(1)

	if err != nil || len(outputRoutineExercisesIds) != 2 || outputRoutineExercisesIds[0] != 1 {
		log.Printf("output routine exercises length, %t", len(outputRoutineExercisesIds) != 2)
		log.Printf("err %s", err.Error())
		t.Fatalf("Error is present, or wrong number of routine exercises found")
	}
}

func TestWhenDeleteRoutinExercisesByRoutineCalled_ThenNoErrorIsReturned(t *testing.T) {
	db, mock, err := SetUpDbMock()
	if err != nil {
		t.Fatal("Failed to open mock db connection", err)
	}
	repo := RoutineExerciseRepository{DB: db}
	deleteStatement := "UPDATE `routine_exercises` SET `deleted_at`=? WHERE routine_id = ? AND `routine_exercises`.`deleted_at` IS NULL"
	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta(deleteStatement)).
		WithArgs(AnyTime{}, 1).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	err = repo.DeleteRoutineExercisesByRoutine(1)
	if err != nil {
		t.Fatalf("Error has occurred")
	}
}
