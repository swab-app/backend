module gitlab.com/swab-app/backend

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0 // indirect
	github.com/auth0/go-jwt-middleware v0.0.0-20190805220309-36081240882b
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/rs/cors v1.7.0
	golang.org/x/crypto v0.0.0-20210506145944-38f3c27a63bf
	gorm.io/driver/mysql v1.0.5
	gorm.io/gorm v1.21.8
)
