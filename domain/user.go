package domain

import (
	"log"
	"time"

	"gitlab.com/swab-app/backend/util"
	"gorm.io/gorm"
)

/**
Hey, you added the unique constraint to user and email, and it's crashes the app lmao
*/
type User struct {
	ID        uint           `gorm:"primaryKey" json:"id"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deleted_at"`
	UserName  string         `gorm:"not null;unique;column:username" json:"username"`
	Email     string         `gorm:"not null;unique" json:"email"`
	Password  util.Password
}

func (u *User) Hash() string {
	hashedPass, error := u.Password.Hash()
	if error != nil {
		log.Printf("Error hashing pass: %s\n", error)
	}
	return hashedPass
}
