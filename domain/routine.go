package domain

import (
	"time"

	"gorm.io/gorm"
)

type Routine struct {
	ID               uint           `gorm:"primaryKey" json:"id"`
	CreatedAt        time.Time      `json:"created_at"`
	UpdatedAt        time.Time      `json:"updated_at"`
	DeletedAt        gorm.DeletedAt `gorm:"index" json:"deleted_at"`
	Name             string         `json:"name"`
	UserID           int
	User             User
	RoutineExercises []RoutineExercise `json:"routineExercises"`
}

type RoutineExercise struct {
	ID         uint `gorm:"primaryKey" json:"id"`
	ExerciseId int
	RoutineId  int
	Order      int
	Sets       []Set    `json:"sets"`
	Routine    Routine  `json:"routine"`
	Exercise   Exercise `json:"exercise"`
	CreatedAt  time.Time
	UpdatedAt  time.Time
	DeletedAt  gorm.DeletedAt
}
