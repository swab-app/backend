package domain

import "gorm.io/gorm"

func Migrate(db *gorm.DB) {
	db.AutoMigrate(&User{})
	db.AutoMigrate(&Routine{})
	db.AutoMigrate(&Workout{})
	db.AutoMigrate(&WorkoutExercise{})
	db.AutoMigrate(&Exercise{})
	db.AutoMigrate(&RoutineExercise{})
	db.AutoMigrate(&Set{})

	fillInExercises(db)
}

func fillInExercises(db *gorm.DB) {
	result := db.First(&Exercise{})
	if result.RowsAffected == 0 {
		exercises := []Exercise{
			{
				Name: "Overhead Press",
				Type: REPS,
			},
			{
				Name: "Squat",
				Type: REPS,
			},
			{
				Name: "Deadlift",
				Type: REPS,
			},
			{
				Name: "Bench",
				Type: REPS,
			},
			{
				Name: "Pull-Up",
				Type: REPS,
			},
		}

		db.Create(exercises)
	}
}
