package domain

import (
	"time"

	"gorm.io/gorm"
)

type ExerciseType string

const (
	REPS     ExerciseType = "REPS"
	TIMED    ExerciseType = "TIMED"
	DISTANCE ExerciseType = "DISTANCE"
)

type Exercise struct {
	ID        uint           `gorm:"primaryKey" json:"id"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deleted_at"`
	Name      string         `json:"name"`
	Type      ExerciseType   `json:"type"`
}
