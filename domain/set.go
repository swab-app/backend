package domain

import (
	"time"

	"gorm.io/gorm"
)

type Set struct {
	ID                uint           `gorm:"primaryKey" json:"id"`
	CreatedAt         time.Time      `json:"created_at"`
	UpdatedAt         time.Time      `json:"updated_at"`
	DeletedAt         gorm.DeletedAt `gorm:"index" json:"deleted_at"`
	RoutineExerciseId *int
	RoutineExercise   RoutineExercise
	Type              ExerciseType
	WorkoutExerciseId *int
	WorkoutExercise   WorkoutExercise
	Weight            float32 `sql:"type:demical(8,4)" json:"weight"`
	Time              int     `json:"time"`
	Reps              int     `json:"reps"`
	Distance          float32 `sql:"type:demical(8,4)" json:"distance"`
	DistanceUnit      string  // refactor into a enum type thing.
}
