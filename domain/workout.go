package domain

import (
	"time"

	"gorm.io/gorm"
)

type Workout struct {
	ID               uint           `gorm:"primaryKey" json:"id"`
	CreatedAt        time.Time      `json:"created_at"`
	UpdatedAt        time.Time      `json:"updated_at"`
	DeletedAt        gorm.DeletedAt `gorm:"index" json:"deleted_at"`
	Name             string         `json:"name"`
	StartTime        time.Time      `json:"startTime"`
	EndTime          time.Time      `json:"endTime"`
	UserID           int
	User             User
	RoutineId        int
	Routine          Routine
	WorkoutExercises []WorkoutExercise `json:"workoutExercises"`
}

type WorkoutExercise struct {
	ID         uint `gorm:"primaryKey" json:"id"`
	ExerciseId int
	WorkoutId  int
	Order      int
	Sets       []Set    `json:"sets"`
	Workout    Workout  `json:"workout"`
	Exercise   Exercise `json:"exercise"`
	CreatedAt  time.Time
	UpdatedAt  time.Time
	DeletedAt  gorm.DeletedAt
}
