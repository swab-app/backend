package util

import (
	"golang.org/x/crypto/bcrypt"
)

type Password string

func (p Password) Hash() (string, error) {
	bytes, error := bcrypt.GenerateFromPassword([]byte(p), bcrypt.DefaultCost)
	return string(bytes), error
}

func (p Password) CheckAgainstHash(hash string) bool {
	error := bcrypt.CompareHashAndPassword([]byte(hash), []byte(p))
	return error == nil
}

// Marshaler ignores the field value completely.
func (Password) MarshalJSON() ([]byte, error) {
	return []byte(`""`), nil
}
