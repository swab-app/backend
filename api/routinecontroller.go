package api

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/swab-app/backend/application"
	"gitlab.com/swab-app/backend/dtos"
)

type RoutineController struct {
	RoutineService application.IRoutineService
}

func (rc *RoutineController) RetrieveRoutines(w http.ResponseWriter, r *http.Request) {
	userId := getUserIdInt(r)
	routines, err := rc.RoutineService.GetRoutinesByUserId(userId)

	if err != nil {
		log.Print("Failed to retrieve routine", err)
		respondInternalServerError(w)
		return
	}
	if len(routines) == 0 {
		respondJson(w, http.StatusNotFound, "No routines found")
		return
	}

	respondJson(w, http.StatusOK, routines)
}

func (rc *RoutineController) GetRoutine(w http.ResponseWriter, r *http.Request) {
	userId := getUserIdInt(r)
	routineId, err := strconv.Atoi(mux.Vars(r)["id"])

	if err != nil {
		log.Printf("Invalid url param %s", err.Error())
		respondBadRequest(w)
		return
	}

	routine, err := rc.RoutineService.GetRoutineByIdAndUserId(routineId, userId, true)

	if err != nil {
		log.Print("Failed to retrieve routine", err)
		respondInternalServerError(w)
		return
	}

	if routine.ID == 0 {
		respondJson(w, http.StatusNotFound, "No routines found")
		return
	}

	respondJson(w, http.StatusOK, routine)
}

func (rc *RoutineController) CreateRoutine(w http.ResponseWriter, r *http.Request) {
	var createRoutineDto dtos.RoutineCreateRequestDto
	err := decodeBody(w, r, &createRoutineDto)

	if err != nil {
		log.Printf("Failed to decode routine %s", err.Error())
		respondBadRequest(w)
		return
	}

	routine := rc.RoutineService.CreateRoutineFromDto(getUserIdInt(r), createRoutineDto)
	err = rc.RoutineService.CreateRoutine(routine)

	if err != nil {
		log.Print("Failed saving routine:", err.Error())
		respondInternalServerError(w)
		return
	}

	respondJson(w, http.StatusOK, routine)
}

func (rc *RoutineController) UpdateRoutine(w http.ResponseWriter, r *http.Request) {
	var updateRoutineDto dtos.RoutineUpdateRequestDto
	err := decodeBody(w, r, &updateRoutineDto)
	if err != nil {
		log.Printf("Failed to decode routine %s", err.Error())
		respondBadRequest(w)
		return
	}

	routineId, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		log.Printf("Invalid url param %s", err.Error())
		respondBadRequest(w)
		return
	}

	routine, err := rc.RoutineService.GetRoutineByIdAndUserId(routineId, getUserIdInt(r), false)
	if err != nil {
		log.Print("Failed retrieving routine:", err.Error())
		respondInternalServerError(w)
		return
	}

	// create updated routine model
	routine = rc.RoutineService.UpdateRoutineFromDto(routine, updateRoutineDto)

	// Find and Delete RoutineExercises and Sets. Really Ugly.
	err = rc.RoutineService.UpdateRoutine(routine)
	if err != nil {
		log.Print("Failed saving routine:", err.Error())
		respondInternalServerError(w)
		return
	}

	respondJson(w, http.StatusOK, routine)
}
