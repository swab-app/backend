package api

import (
	"log"
	"net/http"

	"gitlab.com/swab-app/backend/application"
	"gitlab.com/swab-app/backend/dtos"
)

type ExerciseController struct {
	ExerciseService application.IExerciseService
}

func (ex *ExerciseController) SearchExercises(w http.ResponseWriter, r *http.Request) {
	search := dtos.NewExerciseSearchRequestDto(r.URL)
	exercises, err := ex.ExerciseService.SearchExercises(search.Name, search.Type)

	if err != nil {
		log.Printf("Failed se db %s", err.Error())
		respondInternalServerError(w)
		return
	}
	respondJson(w, http.StatusOK, exercises)
}
