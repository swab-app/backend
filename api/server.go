package api

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/swab-app/backend/application"
	"gitlab.com/swab-app/backend/domain"
	"gitlab.com/swab-app/backend/infrastructure"
	"gorm.io/gorm"
)

type Server struct {
	Router *mux.Router
	DB     *gorm.DB
	JwtKey string
}

func (s *Server) SetUpRoutes() {
	// init repositories
	userRepository := infrastructure.UserRepository{DB: s.DB}
	exerciseRepository := infrastructure.ExerciseRepository{DB: s.DB}
	routineRepository := infrastructure.RoutineRepository{DB: s.DB}
	routineExerciseRepository := infrastructure.RoutineExerciseRepository{DB: s.DB}
	setRepository := infrastructure.SetRepository{DB: s.DB}
	workoutRepository := infrastructure.WorkoutRepository{DB: s.DB}

	// init services
	userService := application.UserService{Repository: userRepository, JwtKey: s.JwtKey}
	exerciseService := application.ExerciseService{Repository: exerciseRepository}
	routineService := application.RoutineService{
		RoutineRepository:         routineRepository,
		RoutineExerciseRepository: routineExerciseRepository,
		SetRepository:             setRepository,
	}
	workoutService := application.WorkoutService{Repository: workoutRepository}

	// init controllers
	authController := AuthController{UserService: userService}
	routineController := RoutineController{RoutineService: routineService}
	exerciseController := ExerciseController{ExerciseService: exerciseService}
	workoutController := WorkoutController{WorkoutService: workoutService}

	// init middleware
	jwtMiddleware := AuthMiddleware{JwtKey: s.JwtKey}

	//Routes Public
	s.Router.HandleFunc("/register", authController.Register).Methods("POST")
	s.Router.HandleFunc("/login", authController.Login).Methods("POST")

	// Routes Authenticated
	api := s.Router.PathPrefix("/api").Subrouter()
	api.HandleFunc("/routine", routineController.CreateRoutine).Methods("POST")
	api.HandleFunc("/routine/{id:[0-9]+}", routineController.GetRoutine).Methods("GET")
	api.HandleFunc("/routine/{id:[0-9]+}", routineController.UpdateRoutine).Methods("POST")
	api.HandleFunc("/routine", routineController.RetrieveRoutines).Methods("GET")
	api.HandleFunc("/exercise", exerciseController.SearchExercises).Methods("GET")
	api.HandleFunc("/workout", workoutController.CreateWorkout).Methods("POST")
	api.Use(jwtMiddleware.Protect)
}

func (s *Server) Initialise() error {
	conf := infrastructure.NewConfig()
	log.Printf("Starting %s", conf.AppName)

	// JWT Key
	s.JwtKey = conf.JwtKey

	// connect to db, migrate domain.
	db, err := conf.GetDatabase()
	if err != nil {
		return err
	}
	s.DB = db
	domain.Migrate(s.DB)

	// routes, public and private.
	s.Router = mux.NewRouter()
	s.SetUpRoutes()
	return nil
}

func (s *Server) Run(host string) {
	corsConfig := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{
			http.MethodGet,
			http.MethodPut,
			http.MethodPost,
			http.MethodDelete,
		},
		AllowedHeaders: []string{"*"},
	})
	log.Print(http.ListenAndServe(host, corsConfig.Handler(s.Router)))
}
