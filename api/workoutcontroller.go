package api

import (
	"log"
	"net/http"

	"gitlab.com/swab-app/backend/application"
	"gitlab.com/swab-app/backend/dtos"
)

type WorkoutController struct {
	WorkoutService application.IWorkoutService
}

func (wc *WorkoutController) CreateWorkout(w http.ResponseWriter, r *http.Request) {
	var createWorkoutDto dtos.WorkoutCreateRequestDto
	err := decodeBody(w, r, &createWorkoutDto)

	if err != nil {
		log.Printf("Failed to decode workout %s", err.Error())
		respondBadRequest(w)
		return
	}

	workout := wc.WorkoutService.CreateWorkoutFromDto(getUserIdInt(r), createWorkoutDto)
	err = wc.WorkoutService.CreateWorkout(workout)

	if err != nil {
		log.Printf("Failed to save workout %s", err.Error())
		respondInternalServerError(w)
		return
	}

	respondJson(w, http.StatusOK, workout)
}
