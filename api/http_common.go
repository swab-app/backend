package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

// 32 MB
const maxUpload = 32 * 1024 * 1024

func respondJson(w http.ResponseWriter, status int, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		log.Printf("error decoding payload: %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write([]byte(response))
}

func respondError(w http.ResponseWriter, status int, message string) {
	respondJson(w, status, map[string]string{"error": message})
}

func respondBadRequest(w http.ResponseWriter, message_optional ...string) {
	message := "Bad Request"
	if len(message_optional) > 0 {
		message = message_optional[0]
	}
	respondError(w, http.StatusBadRequest, message)
}

func respondInternalServerError(w http.ResponseWriter, message_optional ...string) {
	message := "Request has failed"
	if len(message_optional) > 0 {
		message = message_optional[0]
	}
	respondError(w, http.StatusInternalServerError, message)
}

// pinching from this https://www.alexedwards.net/blog/how-to-properly-parse-a-json-request-body
func decodeBody(w http.ResponseWriter, r *http.Request, model interface{}) error {
	headerValue := r.Header.Get("Content-Type")
	if headerValue != "" && headerValue != "application/json" {
		message := "Content-Type is not application/json"
		respondError(w, http.StatusUnsupportedMediaType, message)
	}

	r.Body = http.MaxBytesReader(w, r.Body, maxUpload)

	decode := json.NewDecoder(r.Body)
	decode.DisallowUnknownFields()

	err := decode.Decode(&model)
	if err != nil {
		var syntaxError *json.SyntaxError
		var unmarshalTypeError *json.UnmarshalTypeError

		switch {
		case errors.As(err, &syntaxError):
			msg := fmt.Sprintf("Request body contains badly-formed JSON (at position %d)", syntaxError.Offset)
			respondError(w, http.StatusBadRequest, msg)
			return err

		case errors.Is(err, io.ErrUnexpectedEOF):
			respondError(w, http.StatusBadRequest, "Request body contains badly-formed JSON")
			return err

		case errors.As(err, &unmarshalTypeError):
			msg := fmt.Sprintf("Request body contains an invalid value for the %q field (at position %d)", unmarshalTypeError.Field, unmarshalTypeError.Offset)
			respondError(w, http.StatusBadRequest, msg)
			return err

		case strings.HasPrefix(err.Error(), "json: unknown field "):
			fieldName := strings.TrimPrefix(err.Error(), "json: unknown field ")
			msg := fmt.Sprintf("Request body contains unknown field %s", fieldName)
			respondError(w, http.StatusBadRequest, msg)
			return err

		case errors.Is(err, io.EOF):
			respondError(w, http.StatusBadRequest, "Request body must not be empty")
			return err

		case err.Error() == "http: request body too large":
			respondError(w, http.StatusBadRequest, "Request body must not be larger than 32MB")
			return err

		default:
			return err
		}
	}

	/*Tries again to decode, should be nothing else in the reader*/
	err = decode.Decode(&struct{}{})
	if err != io.EOF {
		msg := "Request body must only contain a single JSON object"
		respondError(w, http.StatusBadRequest, msg)
		return err
	}

	return nil
}

// func getUserId(r *http.Request) string {
// 	userId := strconv.Itoa(getUserIdInt(r))
// 	return userId
// }

func getUserIdInt(r *http.Request) int {
	log.Printf("%s", r.Context().Value("user"))
	return int(r.Context().Value("user").(*jwt.Token).
		Claims.(jwt.MapClaims)["user"].(float64))
}
