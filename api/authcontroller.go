package api

import (
	"log"
	"net/http"
	"strings"

	"gitlab.com/swab-app/backend/application"
	"gitlab.com/swab-app/backend/domain"
	"gitlab.com/swab-app/backend/dtos"
	"gitlab.com/swab-app/backend/util"
)

type AuthController struct {
	UserService application.IUserService
}

func (a *AuthController) Login(w http.ResponseWriter, r *http.Request) {
	var login dtos.LoginRequestDto
	err := decodeBody(w, r, &login)
	if err != nil {
		log.Printf("Failed to decode login %s", err.Error())
		respondInternalServerError(w)
		return
	}

	user, err := a.UserService.GetUserByUserName(login.UserName)
	if err != nil {
		if strings.Contains(err.Error(), "username or password") {
			respondJson(w, http.StatusUnauthorized, "User or password is incorrect")
		} else {
			respondInternalServerError(w)
		}
		return
	}

	success := a.UserService.AttemptAuth(user, login.Password)
	if !success {
		respondJson(w, http.StatusUnauthorized, "User or password is incorrect")
		return
	}

	token, err := a.UserService.GetToken(user)
	if err != nil {
		respondInternalServerError(w)
		return
	}

	response := dtos.LoginResponseDto{
		User:  user,
		Token: token,
	}
	respondJson(w, http.StatusOK, response)
}

func (a *AuthController) Register(w http.ResponseWriter, r *http.Request) {
	// decode a user
	var registerRequest dtos.RegisterRequestDto
	err := decodeBody(w, r, &registerRequest)
	if err != nil {
		log.Printf("Failed to decode register request: %s", err.Error())
		respondInternalServerError(w)
		return
	}

	if registerRequest.Password != registerRequest.PasswordConfirmation {
		respondBadRequest(w, "Passwords do not match")
		return
	}

	var user = domain.User{
		UserName: registerRequest.UserName,
		Password: util.Password(registerRequest.Password),
		Email:    registerRequest.Email,
	}

	user, err = a.UserService.CreateUser(user)
	if err != nil {
		log.Printf("Failed to save user to db: %s", err.Error())
		//todo: return something the frontend can use to say its not unique
		respondInternalServerError(w)
		return
	}

	token, err := a.UserService.GetToken(user)
	if err != nil {
		log.Printf("Failed to generate token for user %d", user.ID)
		respondInternalServerError(w)
		return
	}
	loginResponse := dtos.LoginResponseDto{
		User:  user,
		Token: token,
	}
	respondJson(w, http.StatusOK, loginResponse)
}
